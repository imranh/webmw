import axios from 'axios'

export default {
  methods: {
    getImage: function (url) {
      return new Promise((resolve, reject) => {
        axios.get('https://chat.sucs.org/php/imageproxy.php', { params: { url: url } })
        .then(function (res) {
          resolve(res.data)
        }).catch(function (error) {
          console.log(error)
        })
      })
    }
  }
}

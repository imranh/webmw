import axios from 'axios'

export default {
  created: function () {
  },
  methods: {
    sendCmdCallback: function (cmd, callback, err) {
      axios.post('https://chat.sucs.org/php/send.php', { send: cmd, mwsess: this.$ls.get('token') })
      .then(function (response) {
        callback(response)
      }).catch(function (error) {
        console.log(error)
      })
    },
    sendCmd: function (cmd) {
      axios.post('https://chat.sucs.org/php/send.php', { send: cmd, mwsess: this.$ls.get('token') })
    },
    sendSay: function (text) {
      var what = 'say ' + text

      if (text.substr(0, 1) === '.' || text.substr(0, 1) === '!' || text.substr(0, 1) === '/') {
        this.cmdParser(text)
        return
      }

      axios.post('https://chat.sucs.org/php/send.php', { send: what, mwsess: this.$ls.get('token') })

      return false
    },
    cmdParser: function (text) {
      let self = this
      let cmd = ''
      let args = ''
      if (text.search(' ') === -1) {
        cmd = text.substring(1)
      } else {
        cmd = text.substring(1, text.search(' '))
        args = text.substring(text.search(' ') + 1)
      }

      switch (cmd) {
        case 'e':
        case 'em':
        case 'emote':
        case 'me':
          if (typeof args !== undefined) {
            self.sendCmd('emote ' + args)
          }
          break
        case 'sayto':
        case 'sa':
        case 'whisper':
        case 'whi':
          if (typeof args !== undefined) {
            self.sendCmd('sayto ' + args)
          }
          break
      }
    }
  }
}

import axios from 'axios'
import getImage from '../mixins/get-image.js'

let profileImages = {}

export default {
  mixins: [getImage],
  methods: {
    sync: function (ctx, token, callback) {
      axios.get('https://chat.sucs.org/php/poll.php', { params: { mwsess: token }, timeout: 1000 })
      .then((msg) => { ctx.response(msg, callback) }).catch(function (error) {
        if (error.message.indexOf('Network Error') !== -1) {
          window.bus.$emit('online', false)
        } else if (error.message.indexOf('timeout of') !== -1) {
          window.bus.$emit('online', true)
        } else {
          error // 'Handle' the error
        }
      })
    },
    response: async function (msg, callback) {
      if (msg.status !== 200) {
        if (msg.status === 'Socket open error') {
          console.log('Error connecting to socket')
        } else {
          console.log('Problem: ' + msg.status)
        }
      } else {
        window.bus.$emit('msgs-in', (msg.data.length - 1))
        msg = msg.data

        for (var one in msg) {
          window.bus.$emit('msg-done', 1)

          switch (msg[one].state) {
            case 'SAYR':
            case 'SAYU':
              // decode the json message
              var detail = JSON.parse(msg[one].text)

              var body = detail.text
              // make a crude approximation of the old message styles
              switch (detail.type) {
                case 'say':
                  body = detail.text
                  break
                case 'raw':
                  body = detail.text
                  break
                case 'emote':
                  switch (detail.plural) {
                    case 1:
                      body = msg[one].name + '\'s ' + detail.text
                      break
                    case 2:
                      body = msg[one].name + '\' ' + detail.text
                      break
                    case 3:
                      body = msg[one].name + '\'d ' + detail.text
                      break
                    case 4:
                      body = msg[one].name + '\'ll ' + detail.text
                      break
                    default:
                      body = msg[one].name + ' ' + detail.text
                      break
                  }
                  break
                case 'notsayto':
                  body = msg[one].name + ' says (-' + detail.exclude + '): ' + detail.text
                  break
                case 'says':
                  body = msg[one].name + ' says: ' + detail.text
                  break
                case 'whispers':
                  body = msg[one].name + ' whispers: ' + detail.text
                  break
                default:
                  body = msg[one].name + ' ' + detail.text
                  break
              }

              /* Escape HTML characters */
              var escapedMsg = this.filterString(body)
              escapedMsg = this.filterColours(escapedMsg)

              var msgTime = new Date(msg[one].when * 1000)
              var timestamp = this.pad(msgTime.getHours(), 2) + ':' + this.pad(msgTime.getMinutes(), 2)
              msg[one].timestamp = timestamp
              msg[one].msg = escapedMsg

              await this.getHackergotchi(this, msg[one])
              .then(function (msg) {
                var toStore = {
                  _id: msg.serial.toString(),
                  user: msg.name,
                  photo: msg.photo,
                  timestamp: msg.timestamp,
                  rawtimestamp: msg.when,
                  msg: msg.msg
                }

                callback(toStore)
              })
              break

            case 11: // Talker message
            case 17: // Board Message
              /* Escape HTML characters */
              /*
              * For now we're not showing system messages
              escapedMsg = this.filterString(msg[one].text)
              escapedMsg = this.filterColours(escapedMsg)

              msgTime = new Date(msg[one].when * 1000)
              timestamp = this.pad(msgTime.getHours(), 2) + ':' + this.pad(msgTime.getMinutes(), 2)

              var toStore = {
                id_: timestamp,
                timestamp: timestamp,
                msg: escapedMsg
              }

              callback(toStore)
              */
              break

            case 14: // IPC_KICK
              var what = '<div class=\'msgkick user_system\'>'
              if (msg[one].text.substr(0, 1) === 'm') {
                what += 'Boing, Zebedee\'s arrived. "Look up!" says Zebedee<br />'
                what += 'You look up; a large object is falling towards you very fast, very very fast. It looks like a Magic Roundabout!<br />'
                what += '"I wouldn\'t stand there if I was you" says Zebedee<br />'
                what += 'WWWHHHEEEEEEEKKKKEEEERRRRRUUUUUNNNNNCCCCCHHHHHH<br />'
                what += msg[one].name + ' has just dropped the Magic Roundabout of Death on you.<br/>'
              } else {
                what += 'Boing, Zebedee arrived.<br />'
                what += '"Time for bed" says Zebedee<br />'
                what += msg[one].name + ' has just dropped the Zebedee of Death on you.<br />'
              }
              if (msg[one].text.substr(1, 1) === 'r') what += '"' + msg[one].text.substr(2) + '" says Zebedee<br />'
              what += '</div>'

              console.log(what)
              break

            case 23: // CheckOnOff
              // Implement who check
              break

            default:
              if (typeof msg[one].text === 'undefined') {
                // We've been timed out :(
                this.$ls.remove('token')
                this.$clearStorage()
                location.reload()
              }
              console.log('Unknown message type \'' + msg[one].state + '\'')
              console.log(msg[one].text)
              break
          }
        }
      }
    },
    filterString: function (str) {
      var escapedStr = str.replace(/&/g, '&amp;')
      escapedStr = escapedStr.replace(/</g, '&lt;')
      escapedStr = escapedStr.replace(/>/g, '&gt;')
      escapedStr = escapedStr.replace(/\n/g, '')
      escapedStr = escapedStr.replace(/\r/g, '')

      return escapedStr
    },
    filterColours: function (str) {
      /* Replace colour codes with appropriate span tags */
      var msgColours = str.match(/\u001b../g)
      if (msgColours !== null) {
        for (var i = 0; i < msgColours.length; i++) {
          if (isNaN(msgColours[i].charAt(1))) {
            var colourBold = ''
            var fgColour = msgColours[i].charAt(1)
            if (fgColour !== fgColour.toLowerCase()) {
              colourBold = ' bold'
            }
            str = str.replace(msgColours[i], '</span><span class=\'colourfg' + msgColours[i].charAt(1) + colourBold + ' colourbg' + msgColours[i].charAt(2) + '\'>')
          } else {
            str = str.replace(msgColours[i], '</span><span class=\'colour' + msgColours[i].replace(/\u001b/g, '') + '\'>')
          }
        }
      }
      return str
    },
    pad: function (number, length) {
      var str = '' + number
      while (str.length < length) {
        str = '0' + str
      }
      return str
    },
    getHackergotchi: function (ctx, user) {
      var username = user.name
      return new Promise((resolve, reject) => {
        if (typeof profileImages[username] === 'undefined') {
          var hackergotchi = 'https://sucs.org/pictures/people/' + username.toLowerCase() + '.png'
          ctx.getImage(hackergotchi)
          .then(function (image) {
            if (image !== 'not_an_image') {
              profileImages[username] = '<img src="data:{mimetype};base64,' + image + '" width="36px"/>'
            } else {
              profileImages[username] = '<div style="width: 36px; height: 36px; font-size: 7pt; text-align: center; padding-top: 5px; background-color: ' + ctx.stringToColour(username) + '">Don\'t <br>Panic</div>'
            }
            user.photo = profileImages[username]
            resolve(user)
          })
        } else {
          user.photo = profileImages[username]
          resolve(user)
        }
      })
    },
    stringToColour: function (str) {
      var hash = 0

      for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash)
      }

      var colour = '#'
      for (i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF
        colour += ('00' + value.toString(16)).substr(-2)
      }

      return colour
    }
  }
}
